package com.example.carapi.services;

import com.example.carapi.model.Car;
import com.example.carapi.model.FleetAge;
import com.example.carapi.repositories.CarsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.OptionalDouble;

@Service
public class FleetStatisticsService {

    @Autowired
    private CarsRepository repository;

    public FleetAge getAverageFleetAge() {

        List<Car> cars = repository.findAll();

        OptionalDouble average = cars
                .stream()
                .mapToDouble(a -> (2024 - a.getBuild()))
                .average();

        return new FleetAge(cars.size(), average.getAsDouble());
    }
}
